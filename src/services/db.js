import { v4 as uuidv4 } from "uuid";
import singleton from "../utils/singleton";

/**
 * account: { id, name, email }
 * group: { id, name }
 * member: { id, accountId, groupId }
 * message: { id, message, groupId, accountId }
 */

const db = {
  messages: [],
  accounts: [],
  members: [],
  groups: [],
};

function Db() {
  function createAccount(props) {
    const id = uuidv4();
    const account = { ...props, id };
    db.accounts.push(account);
    return Promise.resolve(account);
  }

  function createGroup(props) {
    const id = uuidv4();
    const group = { ...props, id };
    db.groups.push(group);
    return Promise.resolve(group);
  }

  function getMessagesByGroup(groupId) {
    const groupMessages = db.messages.filter((m) => m.groupId === groupId);
    return Promise.resolve(groupMessages);
  }

  return {
    createAccount,
    createGroup,
    getMessagesByGroup,
  };
}

export default singleton(Db);
