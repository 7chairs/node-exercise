import db from "../services/db";

export default {
  create: async (req, res) => {
    console.log("create account", req.body);
    const account = await db.createAccount(req.body);
    res.status(200).json(account);
  },
};
